#!/usr/bin/env sh

valgrind --leak-check=full \
    --show-leak-kinds=all \
    --track-origins=yes \
    --verbose \
    --log-file="$1".log \
    ./"$1" && \
    echo && \
    tail -7 "$1".log
