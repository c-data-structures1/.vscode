#!/usr/bin/env bash

cfile="$1/$2.c"
headerfile="$1/$2.h"
defCheck="$(echo "$2" | awk '{print toupper($1)}')_H"

DNT="// * DNT"
MIDDLE=""
EDNT="// * EDNT"
if [ -f "$headerfile" ]; then
    start=0
    end=0
    while read -r line; do             # Per ogni linea del file
        if [ "$EDNT" = "$line" ]; then # COntrollo se ho trovato la stringa di fine
            end=1
        fi

        if [ $((end)) -eq 1 ]; then # Se ho trovato la fine
            break
        fi

        if [ $((start)) -eq 1 ]; then # Se ho trovato l'inizio
            MIDDLE="${MIDDLE}${line}\n"
        fi

        if [ "$DNT" = "$line" ]; then # Controllo se ho trovato la stringa di inizio
            start=1
        fi
    done < <(cat "$headerfile")
fi

# Remove last \n
MIDDLE="${MIDDLE%?}"
MIDDLE="${MIDDLE%?}"

rm "$headerfile"
makeheaders "$cfile"

{
    echo "#ifndef ${defCheck}"
    echo "#define ${defCheck}"
    echo
    echo "$DNT"
    echo -e "$MIDDLE"
    echo "$EDNT"
    echo
    tail -n +2 "$headerfile"
    echo
    echo "#endif // ! ${defCheck}"
} >.vscode/tempMakeheaders

mv .vscode/tempMakeheaders "$headerfile"
clang-format -i -style=file "$headerfile"
