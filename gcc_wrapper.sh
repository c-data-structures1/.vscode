#!/usr/bin/env sh

getCDeclarations() {
    filename="$(echo "$1" | sed 's|/| |g' | awk '{print $NF}')"     # Filename only
    basedir="${1%$filename}"                                        # File directory path
    filenameNoExtension="$(echo "$filename" | sed 's|\.[A-z]*$||')" # Filename without extension
    extension="${filename##$filenameNoExtension}"                   # Extension

    if [ "$extension" = ".c" ]; then                                                                      # If I'm on a C file
        headerFile="${1%$extension}.h"                                                                    # Path for the header file
        out="$(grep "#include \"" "$1" | awk '{print $2}' | sed "/$filenameNoExtension\.h\"$/d;s/\"//g")" # Includes list without the header
        [ -f "$headerFile" ] && {                                                                         # If header file exist
            if [ -z "$out" ]; then
                out="$(grep "#include \"" "$headerFile" | awk '{print $2}' | sed "s/\"//g")" # Write a new Empty list
            else
                out="$out $(grep "#include \"" "$headerFile" | awk '{print $2}' | sed "s/\"//g")" # Add header's includes to the C file list
            fi
        }
    else
        out="$(grep "#include \"" "$1" | awk '{print $2}' | sed "s/\"//g")" # Print all the includes of the file
    fi
    [ -z "$out" ] && return 0         # Exit if list it's empty
    echo "$out" | sed "s|^|$basedir|" # Add basedir to the includes
}

getIncludeList() {
    [ ! -f "$1" ] && return 1 # If not a file
    cFile="$(echo "$1" | sed 's|\.h$|.c|')"
    [ -f "$cFile" ] && echo "\"$cFile\""      # If the relative c file exist print the path
    includes="$(getCDeclarations "$cFile")"   # Get the includes inside the file
    [ -z "$includes" ] && return 0            # Exit if there aren't new includes
    echo "$includes" | while read -r file; do # For included file
        getIncludeList "$file"                # Get the includes list
    done
}

filesToInclude="$(getIncludeList "$1" | tr '\n' ' ')" # Get includes list and replace newlines with spaces
shift                                                 # Skip the first arg
eval gcc "$@" "$filesToInclude"                       # Compile file
