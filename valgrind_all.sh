#!/usr/bin/env sh

[ -z "$1" ] && testName="test.c" || testName="$1"
filenameNoExt="$(echo "$testName" | sed 's|\.c$||')"
buildPath="builds/$filenameNoExt"
buildLog="$buildPath.log"
filesToTest="$(find . -name "$testName" | sed 's|^\.\/||')"

for file in $filesToTest; do
    ./.vscode/gcc_wrapper.sh "$file" -o "$buildPath"
    valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file="$buildLog" ./"$buildPath" > /dev/null
    errors="$(grep "ERROR SUMMARY:" "$buildLog")"
    echo "Errors for $file:"
    echo "  $errors"
    echo
done
